package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Candidate;
import th.or.set.e_recruit_service.models.params.EducationParams;
import th.or.set.e_recruit_service.models.params.PersonParams;
import th.or.set.e_recruit_service.models.params.PersonSkillParams;
import th.or.set.e_recruit_service.services.CandidateService;

import java.util.List;

@Value
@RestController
public class CandidateController extends CommonController {

  CandidateService candidateService;

  @GetMapping("/candidates")
  public ResponseEntity<List<Candidate>> filterCandidatesByRecruitStatus(
      @RequestParam(value = "person-name", required = false) String personName,
      @RequestParam(value = "recruit-status", required = false) String recruitStatus,
      @RequestParam(value = "job-position", required = false) String jobPosition,
      @RequestParam(value = "education", required = false) String education,
      @RequestParam(value = "gpax", required = false) Double gpax,
      @RequestParam(value = "toeic-score", required = false) Integer toeicScore,
      @RequestParam(value = "toefl-score", required = false) Integer toeflScore,
      @RequestParam(value = "ielts-score", required = false) Integer ieltsScore,
      @RequestParam(required = false) String gender,
      @RequestParam boolean favorited,
      @RequestParam boolean blacklisted,
      @RequestParam(value = "min-age") Integer minAge,
      @RequestParam(value = "max-age") Integer maxAge,
      @RequestParam(value = "min-exp") Integer minExperience,
      @RequestParam(value = "max-exp") Integer maxExperience) {

    PersonParams personParams = new PersonParams();
    PersonSkillParams personSkillParams = new PersonSkillParams();
    EducationParams educationParams = new EducationParams();

    personParams = personParams.toBuilder().personName(personName).gender(gender).favorited(favorited).blacklisted(blacklisted)
        .minAge(minAge).maxAge(maxAge).minExperience(minExperience).maxExperience(maxExperience).build();

    personSkillParams = personSkillParams.toBuilder().toeicScore(toeicScore)
        .toeflScore(toeflScore).ieltsScore(ieltsScore).build();

    educationParams.toBuilder().education(education).gpax(gpax).build();

    System.out.println(personParams + "\n" + personSkillParams);

    List<Candidate> candidates = candidateService.findCandidatesWithFilters(personParams, personSkillParams,
        educationParams, recruitStatus, jobPosition);

    if (candidates.isEmpty()) {
      return new ResponseEntity<>(candidates, HttpStatus.NO_CONTENT);
    } else {
      return new ResponseEntity<>(candidates, HttpStatus.OK);
    }

  }

//  @GetMapping("/candidates/all")
//  public ResponseEntity<List<Candidate>> filterCandidatesByRecruitStatus() {
//
//    List<Candidate> candidates = candidateService.filterCandidatesByStatus(Optional.of(status));
//    if (candidates.isEmpty()) {
//      return new ResponseEntity<>(candidates, HttpStatus.NO_CONTENT);
//    } else {
//      return new ResponseEntity<>(candidates, HttpStatus.OK);
//    }
//
//  }

}
