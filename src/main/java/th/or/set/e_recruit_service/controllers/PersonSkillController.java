package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.PersonSkill;
import th.or.set.e_recruit_service.services.PersonSkillService;

import java.util.List;

@Value
@RestController
public class PersonSkillController extends CommonController {

  PersonSkillService personSkillService;

  @GetMapping("/persons-skills")
  public ResponseEntity<List<PersonSkill>> filterAllPersonsSkillsBySkillScoreParam(
      @RequestParam(value = "toeic-score", required = false) Integer toeicScore,
      @RequestParam(value = "toefl-score", required = false) Integer toeflScore,
      @RequestParam(value = "ielts-score", required = false) Integer ieltsScore
  ) {

    HttpHeaders headers = new HttpHeaders();
    headers.add("x-developer", "Korn");
    headers.add("x-team", "ENS");

    return ResponseEntity.ok().headers(headers).body(personSkillService.filterAllBySkillScores(toeicScore, toeflScore, ieltsScore));

  }

}
