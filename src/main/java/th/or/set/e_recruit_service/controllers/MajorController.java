package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Major;
import th.or.set.e_recruit_service.services.MajorService;

import java.util.List;

@Value
@RestController
public class MajorController extends CommonController {
  MajorService majorService;

  @GetMapping("/majors")
  public ResponseEntity<List<Major>> getAllMajor() {
    try {
      List<Major> majors = majorService.getAllMajor();

      if (majors.isEmpty()) {
        return new ResponseEntity<>(majors, HttpStatus.NO_CONTENT);
      } else {
        return new ResponseEntity<>(majors, HttpStatus.OK);
      }
    } catch (JDBCConnectionException e) {

      e.printStackTrace();
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

    }
  }
}
