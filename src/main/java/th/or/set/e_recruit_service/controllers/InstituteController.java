package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Institute;
import th.or.set.e_recruit_service.services.InstituteService;

import java.util.List;

@Value
@RestController
public class InstituteController extends CommonController {
  InstituteService instituteService;

  @GetMapping("/institutes")
  public ResponseEntity<List<Institute>> getAllInstitutes() {

    try {
      List<Institute> institutes = instituteService.getAllInstitute();

      if (institutes.isEmpty()) {
        return new ResponseEntity<>(institutes, HttpStatus.NO_CONTENT);
      } else {
        return new ResponseEntity<>(institutes, HttpStatus.OK);
      }

    } catch (JDBCConnectionException e) {

      e.printStackTrace();
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

    }
  }
}
