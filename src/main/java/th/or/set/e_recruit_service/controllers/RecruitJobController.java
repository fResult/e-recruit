package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.RecruitJob;
import th.or.set.e_recruit_service.services.RecruitJobService;

@Value
@RestController
public class RecruitJobController extends CommonController {

  RecruitJobService recruitJobService;

  @GetMapping("recruit-jobs")
  public RecruitJob findById(@RequestParam Integer id) {
    return recruitJobService.findById(id);
  }

}
