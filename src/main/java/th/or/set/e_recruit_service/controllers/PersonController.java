package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Person;
import th.or.set.e_recruit_service.models.params.PersonParams;
import th.or.set.e_recruit_service.repositories.PersonRepository;
import th.or.set.e_recruit_service.services.PersonService;

import java.util.List;

@Value
@RestController
public class PersonController extends CommonController {

  PersonService personService;
  PersonRepository personRepository;

  @GetMapping("/persons")
  public ResponseEntity<List<Person>> findAllPersonsWithPersonParams(
      @RequestParam("full-name") String fullName,
      @RequestParam("min-age") Integer minAge,
      @RequestParam("max-age") Integer maxAge,
      @RequestParam("min-exp") Integer minExp,
      @RequestParam("max-exp") Integer maxExp,
      @RequestParam(required = false) String gender,
      @RequestParam boolean favorited,
      @RequestParam boolean blacklisted
  ) {

    PersonParams personParams = new PersonParams(fullName, gender, favorited, blacklisted, minAge, maxAge, minExp, maxExp);

    return new ResponseEntity<>(personService.findAllPersonsWithPersonParams(personParams), HttpStatus.OK);

  }

  @GetMapping("/test")
  public String test() {
    return personRepository.test();
  }

}
