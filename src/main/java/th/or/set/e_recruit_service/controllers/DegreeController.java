package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Degree;
import th.or.set.e_recruit_service.services.DegreeService;

import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Value
@Log4j2
@RestController
public class DegreeController extends CommonController {
  DegreeService degreeService;

  @GetMapping("/degrees")
  public ResponseEntity<List<Degree>> getAllDegrees() {

    try {
      List<Degree> degrees = degreeService.getALlDegree();

      if (degrees.isEmpty()) {
        return (ResponseEntity<List<Degree>>) ResponseEntity.noContent();
      } else {
        return ResponseEntity.ok(degrees);
      }

    } catch (JDBCConnectionException e) {

      e.printStackTrace();
      return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(null);

    }
  }

  @GetMapping("/degrees/{id}")
  public ResponseEntity<Degree> findDegreeById(@PathVariable Integer id) {
    HttpHeaders headers = new HttpHeaders();
    headers.add("x-developer", "Korn");
    headers.add("x-team", "ENS");

    return ResponseEntity.ok().headers(headers).body(degreeService.findDegreeById(id));
  }

  @PostMapping("/degrees/name/{name}")
  public ResponseEntity<Degree> createDegree(@PathVariable String name) {

    try {
      Degree degree = degreeService.createDegree(name);

      if (isNull(degree)) {
        return ResponseEntity.unprocessableEntity().body(null);
      } else {
        return ResponseEntity.ok(degree);
      }

    } catch (CannotCreateTransactionException e) {
      log.info("Server error at createDegree()");
      e.printStackTrace();
      return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(null);
    }
  }

}
