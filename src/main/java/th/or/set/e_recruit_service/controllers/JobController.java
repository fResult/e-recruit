package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import th.or.set.e_recruit_service.domains.Job;
import th.or.set.e_recruit_service.services.JobService;

import java.util.ArrayList;
import java.util.List;

@Value
@RestController
public class JobController extends CommonController {

  JobService jobService;

  @GetMapping("/jobs")
  public List<Job> findAllJobsByNameLike(@RequestParam(required = false) String name) {

    return jobService.findAllJobsByName(name);

  }

}
