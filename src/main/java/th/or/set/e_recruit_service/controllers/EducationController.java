package th.or.set.e_recruit_service.controllers;

import lombok.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import th.or.set.e_recruit_service.domains.Education;
import th.or.set.e_recruit_service.models.params.EducationParams;
import th.or.set.e_recruit_service.services.EducationService;

import java.util.ArrayList;
import java.util.List;

@Value
@RestController
public class EducationController extends CommonController {

  EducationService educationService;

  @GetMapping("/educations")
  public List<Education> filterAllByEducationParams(@RequestParam(required = false) String degree,
                                                                    @RequestParam(required = false) String major,
                                                                    @RequestParam(required = false) String institute,
                                                                    @RequestParam(required = false) Double gpax) {

    EducationParams educationParams = new EducationParams();
    EducationParams.EducationParamsBuilder paramsBuilder = educationParams.toBuilder();

    educationParams = paramsBuilder.education(gpax.toString()).gpax(gpax).build();

    return educationService.filterAllByEducationParams(educationParams);
  }

}
