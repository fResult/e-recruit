package th.or.set.e_recruit_service.controllers.advices;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import th.or.set.e_recruit_service.exception.CommonException;

import java.time.LocalDateTime;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

// TODO Step 37 Exception Handling : Guide user could solve his problem with human readable http response error message
@RestControllerAdvice
public class CommonAdvice extends ResponseEntityExceptionHandler {

  // 1
  @Getter
  @Setter
  private class ExceptionResponse {
    private String code;
    private String message;
    private LocalDateTime timestamp;
  }

  // 2
  private ResponseEntity<Object> handle(String message, HttpStatus status, String code) {

    ExceptionResponse response = new ExceptionResponse();
    response.setCode("ens-" + code);
    response.setMessage(message);
    response.setTimestamp(LocalDateTime.now());

    HttpHeaders headers = new HttpHeaders();
    headers.add("x-developer", "ENS");
    headers.add("x-info", "http://localhost:8081/api/v1/errors/" + code);

    return ResponseEntity.status(status).headers(headers).body(response);
  }

  //3
  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception e, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
    return handle(e.getMessage(), status, String.valueOf(status.value()));
  }


  //4
  @ExceptionHandler(value = { CommonException.class })
  protected ResponseEntity<Object> handleCommonException(CommonException e) {
    return handle(e.getMessage(), e.getStatus(), e.getCode());
  }

  //5
  @ExceptionHandler(value = { Exception.class })
  protected ResponseEntity<Object> handleException(Exception e) {
    return handle(e.getMessage(), INTERNAL_SERVER_ERROR, String.valueOf(INTERNAL_SERVER_ERROR.value()));
  }

}
