package th.or.set.e_recruit_service.converters;

import th.or.set.e_recruit_service.domains.Candidate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static java.util.Objects.isNull;

@Converter(autoApply = true)
public class CandidateStatusConverter implements AttributeConverter<Candidate.Status, String> {

  @Override
  public String convertToDatabaseColumn(Candidate.Status status) {
    return isNull(status) ? null : status.getCode();
  }

  @Override
  public Candidate.Status convertToEntityAttribute(String code) {
    return isNull(code) ? null : Candidate.Status.convertCodeToStatus(code);
  }
}
