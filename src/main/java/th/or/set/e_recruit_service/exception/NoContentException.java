package th.or.set.e_recruit_service.exception;

import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class NoContentException extends CommonException {

  HttpStatus status = HttpStatus.NO_CONTENT;
  String code = "204";

  public NoContentException(String message) {
    super(message);
  }

}
