package th.or.set.e_recruit_service.exception;

import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
public class UnAuthorizedException extends CommonException {

  HttpStatus status = HttpStatus.UNAUTHORIZED;
  String code = "401";

  public UnAuthorizedException(String message) {
    super(message);
  }

}
