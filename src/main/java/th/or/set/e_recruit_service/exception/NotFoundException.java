package th.or.set.e_recruit_service.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Getter
@Setter
//@Value
public class NotFoundException extends CommonException {

  private final HttpStatus status = HttpStatus.NOT_FOUND;
  private final String code = "404";

  public NotFoundException(String message) {
    super(message);
  }

}
