package th.or.set.e_recruit_service.models.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PersonSkillParams {

  private Integer toeicScore;
  private Integer toeflScore;
  private Integer ieltsScore;

}
