package th.or.set.e_recruit_service.models.params;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PersonParams {

  private String personName;
  private String gender;
  private boolean favorited;
  private boolean blacklisted;
  private Integer minAge;
  private Integer maxAge;
  private Integer minExperience;
  private Integer maxExperience;

}
