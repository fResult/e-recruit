package th.or.set.e_recruit_service.models.params;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class EducationParams {

  private String education;
  private Double gpax;

}
