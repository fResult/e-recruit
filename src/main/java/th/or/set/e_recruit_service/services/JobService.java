package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Job;
import th.or.set.e_recruit_service.repositories.JobRepository;

import java.util.List;

@Value
@Service
public class JobService {

  JobRepository jobRepository;

  public List<Job> findAllJobsByName(String name) {
    System.out.println(name + ":::: hello my name.");
    List<Job> jobs =  jobRepository.filterAllJobsByName(name);

    System.out.println(jobs.isEmpty());
    jobs.forEach(job -> System.out.println("yoyo job:: " + job));

    return jobs;

  }
}
