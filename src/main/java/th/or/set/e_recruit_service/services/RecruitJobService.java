package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.RecruitJob;
import th.or.set.e_recruit_service.repositories.RecruitJobRepository;

import java.util.List;
import java.util.Optional;

@Value
@Service
public class RecruitJobService {

  RecruitJobRepository recruitJobRepository;

  public RecruitJob findById(Integer id) {

    Optional<RecruitJob> optRecruitJob = recruitJobRepository.findById(id);
    RecruitJob recruitJob = null;

    if (optRecruitJob.isPresent()) {
      recruitJob = optRecruitJob.get();
    }

    return recruitJob;


  };

}
