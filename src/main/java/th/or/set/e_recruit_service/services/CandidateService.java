package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Candidate;
import th.or.set.e_recruit_service.domains.Person;
import th.or.set.e_recruit_service.models.params.EducationParams;
import th.or.set.e_recruit_service.models.params.PersonParams;
import th.or.set.e_recruit_service.models.params.PersonSkillParams;
import th.or.set.e_recruit_service.repositories.CandidateRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Value
@Service
public class CandidateService {

  CandidateRepository candidateRepository;
  PersonService personService;

  // TODO 01: Add logic for filter with optional parameters (JPA)
  public List<Candidate> findCandidatesWithFilters(PersonParams personParams, PersonSkillParams personSkillParams,
                                                   EducationParams educationParams, String recruitStatus, String jobName) {

    return candidateRepository.findAllWithFilters(personParams.isFavorited(), personParams.isBlacklisted(), recruitStatus,
        personParams.getMinExperience(), personParams.getMaxExperience(), personParams.getMinAge(), personParams.getMaxAge(),
        personSkillParams.getToeicScore(), personSkillParams.getToeflScore(), personSkillParams.getIeltsScore(),
        personParams.getGender(), personParams.getPersonName(), educationParams.getGpax(),
        educationParams.getEducation(), jobName);

  }

  public List<Candidate> filterCandidatesByStatus(Optional<String> optStatus) {

    List<Person> persons = new ArrayList<>();
    if (optStatus.isPresent()) {
//      persons = personService.findAllWithFilters(Enum.valueOf(Person.Status.class, optStatus.get().toUpperCase()));
    }
//    candidateRepository.findAllWithFilter(1);

    List<Candidate> candidates = new ArrayList<>();

    persons.forEach(person ->
        candidates.addAll((candidateRepository.findAllByPerson(person)))
    );

    return candidates;

  }

}
