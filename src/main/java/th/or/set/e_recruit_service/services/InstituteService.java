package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Institute;
import th.or.set.e_recruit_service.repositories.InstituteRepository;

import java.util.List;

@Value
@Service
public class InstituteService {
    InstituteRepository instituteRepository;

    public List<Institute> getAllInstitute() {
        List<Institute> institutes = instituteRepository.findAll();
        institutes.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        return institutes;
    }
}
