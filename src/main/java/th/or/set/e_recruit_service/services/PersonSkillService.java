package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Person;
import th.or.set.e_recruit_service.domains.PersonSkill;
import th.or.set.e_recruit_service.repositories.PersonSkillRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Value
@Service
public class PersonSkillService {

  PersonSkillRepository personSkillRepository;

  public List<PersonSkill> filterAllBySkillScores(Integer toeicScore, Integer toeflScore, Integer ieltsScore) {

    List<PersonSkill> personSkills = personSkillRepository.filterAllBySkillScores(toeicScore, toeflScore, ieltsScore);
    List<PersonSkill> checker = personSkillRepository.findAllByPerson(personSkills.get(0).getPerson());

    // TODO Refactoring this logic : CHECK TOEIC TOEFL IELTS are contain in the personSkill list
    if (personSkills.size() == checker.size()) {
      return personSkills;
    } else {
      return new ArrayList<>();
    }

  }

  public List<PersonSkill> findAllByPerson(Person person) {

    return personSkillRepository.findAllByPerson(person);

  }

}
