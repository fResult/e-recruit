package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Major;
import th.or.set.e_recruit_service.repositories.MajorRepository;

import java.util.List;

@Value
@Service
public class MajorService {
    MajorRepository majorRepository;

    public List<Major> getAllMajor() {
        List<Major> majors = majorRepository.findAll();
        majors.sort((a,b) -> a.getName().compareToIgnoreCase(b.getName()));
        return majors;
    }
}
