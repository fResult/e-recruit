package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Education;
import th.or.set.e_recruit_service.models.params.EducationParams;
import th.or.set.e_recruit_service.repositories.EducationRepository;

import java.util.List;

@Value
@Service
public class EducationService {

  EducationRepository educationRepository;

  public List<Education> filterAllByEducationParams(EducationParams educationParams) {

    return educationRepository.filterAllByEducationParams(educationParams.getEducation(), educationParams.getEducation(),
        educationParams.getEducation(), educationParams.getGpax());

  }

}
