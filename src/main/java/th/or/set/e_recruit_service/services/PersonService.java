package th.or.set.e_recruit_service.services;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Person;
import th.or.set.e_recruit_service.models.params.PersonParams;
import th.or.set.e_recruit_service.repositories.PersonRepository;
import th.or.set.e_recruit_service.services.string.StringManagement;

import java.util.List;

@Value
@Service
@RequiredArgsConstructor
public class PersonService {

  PersonRepository personRepository;

  public List<Person> findAllPersonsWithPersonParams(PersonParams personParams) {

    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" + personRepository);
    StringManagement stringMgt = new StringManagement();

      String[] parts = stringMgt.splitStringBySpace(personParams.getPersonName());

      String firstName = parts[0];
      String surName = parts[1];



    return personRepository.filterAllByPersonParams(firstName, surName, personParams.getGender(),
        personParams.getMinAge(), personParams.getMaxAge(), personParams.getMinExperience(),
        personParams.getMaxExperience(), personParams.isFavorited(), personParams.isBlacklisted());
  }

//  public List<Person> findByBlacklisted(Boolean blacklist) {
//    personRepository.findAllByBlacklisted(blacklist);
//  }

}
