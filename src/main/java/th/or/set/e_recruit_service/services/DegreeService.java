package th.or.set.e_recruit_service.services;

import lombok.Value;
import org.springframework.stereotype.Service;
import th.or.set.e_recruit_service.domains.Degree;
import th.or.set.e_recruit_service.repositories.DegreeRepository;

import java.util.List;
import java.util.Optional;

@Value
@Service
public class DegreeService {

  DegreeRepository degreeRepository;

  public List<Degree> getALlDegree() {
    List<Degree> degrees = degreeRepository.findAll();
    degrees.sort((a, b) -> a.getName().compareToIgnoreCase(b.getName()));
    return degrees;
  }

  public Degree findDegreeById(Integer id) {
    Optional<Degree> optDegree = degreeRepository.findById(id);

      return optDegree.get();
  }

  public Degree createDegree(String name) {
    Degree degree = new Degree();
    degree.setName(name);
    return degreeRepository.save(degree);
  }
}
