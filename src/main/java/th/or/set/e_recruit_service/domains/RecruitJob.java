package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;
import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity(name = "recruit_jobs")
@SequenceGenerator(name = "recruit_jobs_seq")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RecruitJob extends CommonDomain {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recruit_jobs_seq")
  @Column(length = 20)
  private Integer id;

  @Column(nullable = false)
  private LocalDateTime startDateForInternal;

  @Column(nullable = false)
  private LocalDateTime startDateForExternal;

  @Column(nullable = false)
  private LocalDateTime endDate;

  @Column(length = 100, nullable = false)
  private String coordinator;

  @Column(length = 3, nullable = false)
  private Integer headCount;

  @ManyToOne(fetch = LAZY)
  @JoinColumn(foreignKey = @ForeignKey(name = "fk_recruit_jobs_job_id"))
  private Job job;

}
