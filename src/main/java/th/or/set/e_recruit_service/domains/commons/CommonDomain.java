package th.or.set.e_recruit_service.domains.commons;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter(AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
@MappedSuperclass
@EntityListeners(value = CommonListeners.class)
public abstract class CommonDomain {

  @Column(nullable = false)
  private LocalDateTime createdDate;

  private LocalDateTime updatedDate;

  @Version
  @Column(nullable = false)
  private Integer version;

}
