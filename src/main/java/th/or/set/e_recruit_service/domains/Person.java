package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(exclude = {"educations", "personSkills"})
@Table(indexes = {
    @Index(name = "idx_persons_citizen_id", columnList = "citizenId", unique = true),
    @Index(name = "idx_persons_first_name", columnList = "firstName"),
    @Index(name = "idx_persons_sur_name", columnList = "surName"),
    @Index(name = "idx_persons_first_name_thai", columnList = "firstNameThai"),
    @Index(name = "idx_persons_sur_name_thai", columnList = "surNameThai")
})
@Entity(name = "persons")
@SequenceGenerator(name = "persons_seq")
@DynamicUpdate
@DynamicInsert
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Person extends CommonDomain {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "persons_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 13, nullable = false, unique = true)
  private String citizenId;

  @Column(length = 20)
  private String title;

  @Column(length = 100, nullable = false)
  private String firstName;

  @Column(length = 100, nullable = false)
  private String surName;

  @Column(length = 20)
  private String titleThai;

  @Column(length = 100, nullable = false)
  private String firstNameThai;

  @Column(length = 100, nullable = false)
  private String surNameThai;

  @Column(nullable = false)
  private LocalDate dateOfBirth;

  @Column(length = 1, nullable = false)
  private String gender;

  @Column(length = 20, nullable = false)
  private String phone1;

  @Column(length = 20)
  private String phone2;

  @Column(length = 20)
  private String phone3;

  @Column(length = 40, nullable = false)
  private String email;

  @Column(length = 50)
  private String nationality;

  @Column(length = 50)
  private String race;

  @Column(length = 50)
  private String religion;

  @Column(length = 20)
  private String maritalStatus;

  @Column(length = 100, nullable = false)
  private String emergencyContactName;

  @Column(length = 20, nullable = false)
  private String emergencyContactPhone;

  @Column(length = 100)
  private String referPersonName;

  @Column(length = 20)
  private String referPersonPhone;

  @Column(length = 100)
  private String referLastPosition;

  @Column(length = 100)
  private String referCompany;

  @Column(length = 200)
  private String presentAddress;

  @Column(length = 50)
  private String presentProvince;

  @Column(length = 10)
  private String presentZipCode;

  @Column(length = 200)
  private String permanentAddress;

  @Column(length = 50)
  private String permanentProvince;

  @Column(length = 10)
  private String permanentZipCode;

  @Column(nullable = false)
  @Lob
  private byte[] image;

  @Column(nullable = false)
  @Lob
  private byte[] cv;

  @Column(nullable = false)
  @Lob
  private byte[] transcript;

  private Integer expectedSalary;

  @Column(nullable = false)
  private Integer experienceYear;

  @Column(nullable = false)
  private boolean favorited;

  @Column(nullable = false)
  private boolean blacklisted;

  @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
  private List<Education> educations;

  @OneToMany(mappedBy = "person", cascade = CascadeType.ALL)
  private List<PersonSkill> personSkills;

}
