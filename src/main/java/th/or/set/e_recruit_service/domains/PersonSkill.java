package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity(name = "persons_skills")
@SequenceGenerator(name = "persons_skills_seq")
public class PersonSkill extends CommonDomain {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "persons_skills_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 50, nullable = false)
  private String name;

  @Column(length = 3)
  private Integer score;

  @JsonIgnore
  @ManyToOne(fetch = LAZY)
  @JoinColumn(foreignKey = @ForeignKey(name = "fk_persons_skills_person_id"))
  private Person person;

}
