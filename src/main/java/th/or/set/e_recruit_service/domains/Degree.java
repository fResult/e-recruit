package th.or.set.e_recruit_service.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "degrees")
@Builder(toBuilder = true)
@SequenceGenerator(name = "degrees_seq")
public class Degree {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "degrees_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 100)
  private String name;

}
