package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.stream.Stream;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity(name = "candidates")
@SequenceGenerator(name = "candidates_seq")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Candidate extends CommonDomain {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "candidates_seq")
  @Column(length = 20)
  private Integer id;

  @Column(nullable = false)
  private LocalDateTime appliedDate;

  @Column(length = 3, nullable = false)
  private Candidate.Status status;

  @Column(length = 200)
  private String rejectStatus;

  private boolean readFlag;

  @ManyToOne(fetch = LAZY)
  @JoinColumn(foreignKey = @ForeignKey(name = "fk_candidates_person_id"))
  private Person person;

  @ManyToOne(fetch = LAZY)
  @JoinColumn(foreignKey = @ForeignKey(name = "fk_candidates_recruit_job_id"))
  private RecruitJob recruitJob;

  @PrePersist
  private void prePersist() {
    setAppliedDate(LocalDateTime.now());
  }

  @RequiredArgsConstructor
  public enum Status {
    NEW("NEW"),
    RECRUITER_SCREENING("RES"),
    BP_SCREENING("BPS"),
    BUSINESS_SCREENING("BUS"),
    INTERVIEW("INT"),
    FINAL("FIN"),
    OFFER("OFF"),
    HIRING("HIR");

    @Getter
    private final String code;

    public static Status convertCodeToStatus(String code) {
      return Stream.of(values()).parallel()
          .filter(status -> status.getCode().equalsIgnoreCase(code))
          .findAny().orElseThrow(() ->
              new IllegalArgumentException("The code" + code + " is invalid")
          );
    }
  }
}

