package th.or.set.e_recruit_service.domains.commons;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class CommonListeners<T extends CommonDomain> {

  @PrePersist
  private void prePersist(T e) {
    e.setCreatedDate(LocalDateTime.now());
  }

  @PreUpdate
  private void preUpdate(T e) {
    e.setUpdatedDate(LocalDateTime.now());
  }

}
