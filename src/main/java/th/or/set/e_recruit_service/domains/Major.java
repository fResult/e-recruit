package th.or.set.e_recruit_service.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "majors")
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@SequenceGenerator(name = "majors_seq")
public class Major {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "majors_seq")
    @Column(length = 20)
    private Integer id;

    @Column(length = 100)
    private String name;

}
