package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

import static javax.persistence.FetchType.LAZY;

@Data
@Entity(name = "educations")
@SequenceGenerator(name = "educations_seq")
public class Education extends CommonDomain {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "educations_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 60, nullable = false)
  private String degree;

  @Column(length = 100, nullable = false)
  private String major;

  @Column(length = 100, nullable = false)
  private String institute;

  @Column(nullable = false)
  @Digits(integer = 1, fraction = 2)
  private BigDecimal gpax;

  @JsonIgnore
  @ManyToOne(fetch = LAZY)
  @JoinColumn(foreignKey = @ForeignKey(name = "fk_educations_person_id"))
  private Person person;

}
