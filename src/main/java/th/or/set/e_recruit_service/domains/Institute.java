package th.or.set.e_recruit_service.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity(name = "institutes")
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@SequenceGenerator(name = "institutes_seq")
public class Institute {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "institutes_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 100)
  private String name;

}
