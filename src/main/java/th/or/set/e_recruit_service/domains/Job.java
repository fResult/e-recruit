package th.or.set.e_recruit_service.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;
import th.or.set.e_recruit_service.domains.commons.CommonDomain;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "jobs")
@ToString(exclude = "recruitJobs")
@SequenceGenerator(name = "jobs_seq")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Job extends CommonDomain {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobs_seq")
  @Column(length = 20)
  private Integer id;

  @Column(length = 50, nullable = false)
  private String name;

  @Lob
  private String description;

  @JsonIgnore
  @OneToMany(mappedBy = "job", cascade = CascadeType.ALL)
  private List<RecruitJob> recruitJobs;

}
