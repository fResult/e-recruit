package th.or.set.e_recruit_service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import th.or.set.e_recruit_service.domains.*;
import th.or.set.e_recruit_service.repositories.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Log4j2
@RequiredArgsConstructor
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class ERecruitServiceApplication implements CommandLineRunner {

  private final EducationRepository educationRepository;
  private final PersonRepository personRepository;
  private final JobRepository jobRepository;
  private final PersonSkillRepository personSkillRepository;
  private final RecruitJobRepository recruitJobRepository;
  private final CandidateRepository candidateRepository;
  private final MajorRepository majorRepository;

  public static void main(String... args) {
    System.err.close();
    SpringApplication.run(ERecruitServiceApplication.class, args);
  }

  @Override
  public void run(String... args) {

    Person person = new Person();
    person.setId(1);
    person.setCitizenId("1103700692045");
    person.setTitle("Mr.");
    person.setFirstName("Foo");
    person.setSurName("Bar");
    person.setTitleThai("นาย");
    person.setFirstNameThai("ฟู");
    person.setSurNameThai("บาร์");
    person.setDateOfBirth(LocalDate.of(1991, 11, 5));
    person.setGender("M");
    person.setPhone1("0891441947");
    person.setEmail("foo.bar@foobar.com");
    person.setEmergencyContactName("Jane Doe");
    person.setEmergencyContactPhone("0911441947");
    person.setImage("FooBarImage".getBytes());
    person.setCv("FooBarCv".getBytes());
    person.setTranscript("FooBarTranscript".getBytes());
    person.setExperienceYear(7);
    personRepository.save(person);

    PersonSkill personSkill = new PersonSkill();
    personSkill.setName("TOEIC");
    personSkill.setScore(900);
    personSkill.setPerson(person);
    personSkillRepository.save(personSkill);

    Education education = new Education();
    education.setDegree("Bachelor of Science");
    education.setInstitute("STOU");
    education.setMajor("Computer Science");
    education.setGpax(BigDecimal.valueOf(2.85));
    education.setPerson(person);
    educationRepository.save(education);

    Job job = new Job();
    job.setName("HR Consultant");
    jobRepository.save(job);

    RecruitJob recruitJob = new RecruitJob();
    recruitJob.setStartDateForInternal(LocalDateTime.of(2019, 7, 1, 0, 0));
    recruitJob.setStartDateForExternal(LocalDateTime.of(2019, 7, 1, 0, 0));
    recruitJob.setEndDate(LocalDateTime.of(2019, 7, 31, 23, 59));
    recruitJob.setCoordinator("Kitipong Suksala");
    recruitJob.setHeadCount(100);
    recruitJob.setJob(job);
    recruitJobRepository.save(recruitJob);

    Candidate candidate = new Candidate();
    candidate.setAppliedDate(LocalDateTime.of(2019, 6, 14, 18, 8));
    candidate.setPerson(personRepository.findById(1).get());
    candidate.setRecruitJob(recruitJobRepository.findById(1).get());
    candidate.setPerson(person);
    candidate.setRecruitJob(recruitJob);
    candidate.setStatus(Candidate.Status.HIRING);
    candidateRepository.save(candidate);

//    person.setFavorite(true);
    person.setFirstName("Anakin");
    person.setSurName("Skywalker");
    personRepository.save(person);

//    Degree degree1 = new Degree();
//    degree1 = degree1.toBuilder().name("Degree 1").build();
//    degreeRepository.save(degree1);
//    Degree degree2 = new Degree();
//    degree2 = degree2.toBuilder().name("Degree 2").build();
//    degreeRepository.save(degree2);
//    Degree degree3 = new Degree();
//    degree3 = degree3.toBuilder().name("Degree 3").build();
//    degreeRepository.save(degree3);
//    Degree degree4 = new Degree();
//    degree4 = degree4.toBuilder().name("Degree 4").build();
//    degreeRepository.save(degree4);
//    Degree degree5 = new Degree();
//    degree5 = degree5.toBuilder().name("Degree 5").build();
//    degreeRepository.save(degree5);

//    Institute institute1 = new Institutr

    Major major1 = new Major();
    major1 = major1.toBuilder().name("Major 1").build();
    majorRepository.save(major1);
    Major major2 = new Major();
    major2 = major2.toBuilder().name("Major 2").build();
    majorRepository.save(major2);
    Major major3 = new Major();
    major3 = major3.toBuilder().name("Major 3").build();
    majorRepository.save(major3);
    Major major4 = new Major();
    major4 = major4.toBuilder().name("Major 4").build();
    majorRepository.save(major4);
    Major major5 = new Major();
    major5 = major5.toBuilder().name("Major 5").build();
    majorRepository.save(major5);
  }
}
