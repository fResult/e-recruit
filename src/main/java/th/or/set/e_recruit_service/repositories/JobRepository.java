package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.or.set.e_recruit_service.domains.Job;

import java.util.List;
import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

  @Query(value = "SELECT * FROM jobs " +
      "WHERE (:name = '' OR :name IS NULL OR name LIKE CONCAT('%', :name, '%'))", nativeQuery = true)
  List<Job> filterAllJobsByName(@Param("name") String optName);

}
