package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import th.or.set.e_recruit_service.domains.Candidate;
import th.or.set.e_recruit_service.domains.Person;

import java.util.List;
import java.util.Optional;

public interface CandidateRepository extends JpaRepository<Candidate, Integer> {
  Optional<Candidate> findById(Integer id);

  List<Candidate> findAllByPerson(Person person);

  @Query(value = "SELECT * FROM candidates c " +
      "INNER JOIN persons p ON p.id = c.person_id " +
      "INNER JOIN educations e ON p.id = e.person_id " +
      "INNER JOIN persons_skills ps ON p.id = ps.person_id " +
      "INNER JOIN recruit_jobs rj ON rj.id = p.id " +
      "INNER JOIN jobs j ON j.id = rj.job_id " +
      "WHERE favorited = :favorited AND blacklisted = :blacklisted " +
      " AND (status = :status) " +
      " AND (:minExp <= experience_year AND experience_year <= :maxExp) " +
      " AND (:minAge <= (ROUND(DATEDIFF(NOW(), date_of_birth)/365))" +
      "   AND (ROUND(DATEDIFF(NOW(), date_of_birth)/365) <= :maxAge)) " +
      " AND ((:toeicScore IS NULL OR score >= :toeicScore) " +
      "   OR (:toeflScore IS NULL OR score >= :toeflScore) " +
      "   OR (:ieltsScore IS NULL OR score >= :ieltsScore)) " +
      " AND (:gender = '' OR gender = :gender OR :gender IS NULL) " +
      " AND ((:personName = '' OR first_name LIKE CONCAT('%', :personName, '%') OR :personName IS NULL) " +
      "   OR (:personName = '' OR sur_name LIKE CONCAT('%', :personName, '%') OR :personName IS NULL)) " +
      " AND ((:gpax IS NULL OR gpax >= :gpax) " +
      "   AND ((degree LIKE CONCAT('%', :education, '%') OR :education = '' OR :education IS NULL) " +
      "     OR (major LIKE CONCAT('%', :education, '%') OR :education = '' OR :education IS NULL) " +
      "     OR (institute LIKE CONCAT('%', :education, '%')) OR :education = '' OR :education IS NULL)) " +
      " AND (:jobName = '' OR :jobName IS NULL OR j.name LIKE CONCAT('%', :jobName, '%'));"
      , nativeQuery = true)
  List<Candidate> findAllWithFilters(
      @Param("favorited") boolean favorited, @Param("blacklisted") boolean blacklisted,
      @Param("status") String status, @Param("minExp") Integer minExp, @Param("maxExp") Integer maxExp,
      @Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge, @Param("toeicScore") Integer toeicScore,
      @Param("toeflScore") Integer toeflScore, @Param("ieltsScore") Integer ieltsScore,
      @Param("gender") String gender, @Param("personName") String personName,
      @Param("gpax") Double gpax, @Param("education") String education, @Param("jobName") String jobName
  );

}
