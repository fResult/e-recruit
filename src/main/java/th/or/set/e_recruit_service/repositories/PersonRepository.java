package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.or.set.e_recruit_service.domains.Person;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

  @Query("SELECT p FROM persons p WHERE favorited = :favorited AND blacklisted = :blacklisted " +
      "AND (:gender = '' OR :gender IS NULL OR gender = :gender) " +
      "AND (:minExp <= experience_year AND :maxExp >= experience_year)" +
      "AND ((FUNCTION('ROUND', FUNCTION('DATEDIFF', FUNCTION('NOW'), date_of_birth))/365) >= :minAge " +
      "   AND (FUNCTION('ROUND', FUNCTION('DATEDIFF', FUNCTION('NOW'), date_of_birth))/365) <= :maxAge) " +
      "AND (:firstName = '' OR :firstName IS NULL OR first_name LIKE CONCAT('%' ,:firstName, '%')) " +
      "AND (:surName = '' OR :surName IS NULL OR sur_name LIKE CONCAT('%', :surName, '%')) ")
//  @Query(value = "SELECT * FROM persons p " +
//      "WHERE blacklisted = :blacklisted AND favorited = :favorited " +
//      "AND (:gender = '' OR p.gender = :gender) " +
//      "AND ((:minExp <= p.experience_year AND p.experience_year <= :maxExp)) " +
//      "AND ((:minAge <= ROUND(DATEDIFF(NOW(), p.date_of_birth)/365) " +
//        "AND (:maxAge >= ROUND(DATEDIFF(NOW(), p.date_of_birth)/365)) " +
//      "AND (:firstName = '' OR p.first_name LIKE %:firstName%) " +
//      "AND (:surName = '' OR p.sur_name LIKE %:surName%);",
//      nativeQuery = true)
  List<Person> filterAllByPersonParams(
      @Param("firstName") String firstName, @Param("surName") String surName, @Param("gender") String gender,
      @Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge, @Param("minExp") Integer minExperience,
      @Param("maxExp") Integer maxExperience, @Param("favorited") boolean favorited, @Param("blacklisted") boolean blacklisted
  );

  // FIXME Can be deleted... This method just a test query the Persons by age
  @Query("SELECT FUNCTION('ROUND', (FUNCTION('DATEDIFF', FUNCTION('NOW'), date_of_birth)/365.2525)) FROM persons")
  String test();

}
