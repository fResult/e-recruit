package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.or.set.e_recruit_service.domains.Person;
import th.or.set.e_recruit_service.domains.PersonSkill;

import java.util.List;

@Repository
public interface PersonSkillRepository extends JpaRepository<PersonSkill, Integer> {

  @Query(value = "SELECT * " +
      "FROM persons_skills " +
      "WHERE ((:toeicScore IS NULL OR score >= :toeicScore) AND name = 'TOEIC') " +
      "OR ((:toeflScore IS NULL OR score >= :toeflScore) AND name = 'TOEFL') " +
      "OR ((:ieltsScore IS NULL OR score >= :ieltsScore) AND name = 'IELTS');"
      , nativeQuery = true
  )
  List<PersonSkill> filterAllBySkillScores(
      @Param("toeicScore") Integer toeicScore,
      @Param("toeflScore") Integer toeflScore,
      @Param("ieltsScore") Integer ieltsScore
  );

  List<PersonSkill> findAllByPerson(Person person);

}
