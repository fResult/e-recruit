package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.or.set.e_recruit_service.domains.RecruitJob;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecruitJobRepository extends JpaRepository<RecruitJob, Integer> {

  Optional<RecruitJob> findById(Integer id);

}
