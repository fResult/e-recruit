package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import th.or.set.e_recruit_service.domains.Degree;

import java.util.Optional;


public interface DegreeRepository extends JpaRepository<Degree, Integer> {

  Optional<Degree> findById(Integer integer);
}
