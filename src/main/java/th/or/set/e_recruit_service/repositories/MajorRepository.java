package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import th.or.set.e_recruit_service.domains.Major;

public interface MajorRepository extends JpaRepository<Major, Integer> {
}
