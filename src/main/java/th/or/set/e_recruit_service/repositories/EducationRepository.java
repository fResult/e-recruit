package th.or.set.e_recruit_service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.or.set.e_recruit_service.domains.Education;

import java.util.List;

@Repository
public interface EducationRepository extends JpaRepository<Education, Integer> {

  @Query(value = "SELECT * FROM educations " +
      "WHERE ((:degree = '' OR degree LIKE CONCAT('%', :degree, '%') OR :degree IS NULL) " +
          "OR (:major = '' OR major LIKE CONCAT('%', :major, '%') OR :major IS NULL) " +
          "OR (:institute = '' OR institute LIKE CONCAT('%', :institute, '%') OR :institute IS NULL)) " +
        "AND (:gpax IS NULL OR gpax >= :gpax)"
      , nativeQuery = true)
  List<Education> filterAllByEducationParams(@Param("degree") String degree,
                                             @Param("major") String major,
                                             @Param("institute") String institute,
                                             @Param("gpax") Double gpax);

}
